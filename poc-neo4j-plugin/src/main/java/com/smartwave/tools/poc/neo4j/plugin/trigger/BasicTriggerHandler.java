package com.smartwave.tools.poc.neo4j.plugin.trigger;


import com.smartwave.tools.poc.neo4j.plugin.tools.JsonBuilder;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.graphdb.event.TransactionEventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicTriggerHandler implements TransactionEventHandler {

    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private final static Logger LOGGER = LoggerFactory.getLogger(BasicTriggerExtension.EXTENSION_NAME);


    // =========================================================================
    // before Commit
    // =========================================================================
    @Override
    public Object beforeCommit(final TransactionData transactionData) throws Exception {
        return null;
    }

    // =========================================================================
    // after Commit
    // =========================================================================
    @Override
    public void afterCommit(final TransactionData transactionData, final Object object) {
        final JsonBuilder json = new JsonBuilder();
        json.openObject();

        json.addField("createdNodes");
        json.valueQuot(transactionData.username());
        json.addSeparator();

        json.addField("commitTime");
        json.valueQuot(transactionData.getCommitTime());
        json.addSeparator();

        json.addField("createdNodes");
        json.write(renderJsonNode(transactionData.createdNodes()));
        json.addSeparator();

        json.addField("deletedNodes");
        json.write(renderJsonNode(transactionData.deletedNodes()));
        json.addSeparator();

        json.addField("createdRelationships");
        json.write(renderRelationships(transactionData.createdRelationships()));
        json.addSeparator();

        json.addField("deletedRelationships");
        json.write(renderRelationships(transactionData.deletedRelationships()));

        json.closeObject();

        LOGGER.info(json.toString());


    }


    private String renderJsonNode(final Iterable<Node> nodes) {
        /*
        final JsonBuilder json = new JsonBuilder();
        json.openList();

        if (nodes != null) {
            final Iterator<Node> nodeIterator = nodes.iterator();
            while (nodeIterator.hasNext()) {
                final Node            node           = nodeIterator.next();
                final Iterator<Label> labelsIterator = node.getLabels().iterator();

                json.openObject();
                json.addField("id").write(node.getId()).addSeparator();
                json.addField("labels");
                json.openList();
                while (labelsIterator.hasNext()) {
                    final Label label = labelsIterator.next();
                    json.valueQuot(label.name());
                    if (labelsIterator.hasNext()) {
                        json.addSeparator();
                    }
                }
                json.closeList();
                json.addSeparator();

                json.addField("name");
                json.valueQuot(node.getProperty("name", "undefined"));

                json.closeObject();
                if (nodeIterator.hasNext()) {
                    json.addSeparator();
                }
            }
        }

        json.closeList();
        return json.toString();

         */
        return "";
    }

    private String renderRelationships(final Iterable<Relationship> relationships) {
        /*
        final JsonBuilder json = new JsonBuilder();
        json.openList();

        if (relationships != null) {
            final Iterator<Relationship> relationshipsIterator = relationships.iterator();
            while (relationshipsIterator.hasNext()) {
                final Relationship edge = relationshipsIterator.next();

                json.openObject();
                json.addField("type").valueQuot(edge.getType().name()).addSeparator();
                json.addField("from").write(edge.getStartNodeId()).addSeparator();
                json.addField("to").write(edge.getEndNodeId()).addSeparator();
                json.closeObject();

                if (relationshipsIterator.hasNext()) {
                    json.addSeparator();
                }
            }
        }

        json.closeList();
        return json.toString();

         */
        return "";
    }


    // =========================================================================
    // after Rollback
    // =========================================================================
    @Override
    public void afterRollback(final TransactionData transactionData, final Object object) {

    }


}
