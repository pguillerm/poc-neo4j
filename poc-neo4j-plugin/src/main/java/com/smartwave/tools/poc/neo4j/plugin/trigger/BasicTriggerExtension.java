package com.smartwave.tools.poc.neo4j.plugin.trigger;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.helpers.Service;
import org.neo4j.kernel.configuration.Config;
import org.neo4j.kernel.extension.ExtensionType;
import org.neo4j.kernel.extension.KernelExtensionFactory;
import org.neo4j.kernel.impl.spi.KernelContext;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.FileWriter;
import java.io.Writer;


public class BasicTriggerExtension extends KernelExtensionFactory<BasicTriggerExtension.Dependencies> {


    // =========================================================================
    // ATTRIBUTES
    // =========================================================================

    public static final  String EXTENSION_NAME = "BasicTrigger";
    private static final Logger LOGGER         = LoggerFactory.getLogger(EXTENSION_NAME);
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    protected BasicTriggerExtension(final String key) {


        super(ExtensionType.GLOBAL, EXTENSION_NAME);
        LOGGER.info("some log...");

    }

    @Override
    public Lifecycle newInstance(final KernelContext kernelContext, final Dependencies dependencies) {
        LOGGER.info("{} : newInstance",EXTENSION_NAME);
        return new BasicTriggerExtensionLifecycle(kernelContext, dependencies);
    }


    // =========================================================================
    // LIFECYCLE
    // =========================================================================
    private class BasicTriggerExtensionLifecycle extends LifecycleAdapter {
        private final KernelContext kernelContext;
        private final Dependencies dependencies;
        private final BasicTriggerHandler handler;

        public BasicTriggerExtensionLifecycle(final KernelContext kernelContext, final Dependencies dependencies) {

            this.kernelContext = kernelContext;
            this.dependencies  = dependencies;

            this.handler = new BasicTriggerHandler();
        }
        @Override
        public void init() throws Throwable {
            LOGGER.info("{} : init extension",EXTENSION_NAME);
        }
        @Override
        public void start() throws Throwable {
            LOGGER.info("{} : start extension",EXTENSION_NAME);
            dependencies.getGraphDatabaseService().registerTransactionEventHandler(handler);
        }

        @Override
        public void shutdown() throws Throwable {
            LOGGER.info("{} : shutdown extension",EXTENSION_NAME);
            dependencies.getGraphDatabaseService().unregisterTransactionEventHandler(handler);
        }
    }

    // =========================================================================
    // Dependencies
    // =========================================================================
    public interface Dependencies {
        GraphDatabaseService getGraphDatabaseService();

        Config getConfig();
    }

}
