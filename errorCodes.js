const errors = {
    //=========================================================================
    // TOKEN CHECK
    //=========================================================================
    "1_1_0"   : {statusCode:401, msg:"AWS token is mandatory!",              errorType:"input"},
    "1_1_0_1" : {statusCode:401, msg:"AWS token doesn't start with Bearer!", errorType:"input"},
   
    "1_1_1_0" : {statusCode:401, msg:"AWS token doesn't contains M3_id",                    errorType:"input"},
    "1_1_1_1" : {statusCode:400, msg:"AWS token doesn't contains payload!",                 errorType:"input"},
    "1_1_1_2" : {statusCode:400, msg:"AWS token doesn't contains expiration date!",         errorType:"input"},
    "1_1_1_3" : {statusCode:400, msg:"AWS token doesn't contains aud information!",         errorType:"input"},
    "1_1_1_4" : {statusCode:400, msg:"AWS token doesn't contains iss information!",         errorType:"input"},
    "1_1_1_5" : {statusCode:400, msg:"AWS token doesn't contains token_use information!",   errorType:"input"},
    "1_1_1_6" : {statusCode:400, msg:"AWS token doesn't contains header information!",      errorType:"input"},
    "1_1_1_7" : {statusCode:400, msg:"AWS token doesn't contains kid information!",         errorType:"input"},
    "1_1_1_8" : {statusCode:400, msg:"AWS token doesn't contains algorithms information!",  errorType:"input"},
    "1_1_1_9" : {statusCode:400, msg:"AWS token doesn't contains signature information!",   errorType:"input"},

    "1_1_2_0" : {statusCode:401, msg:"AWS token expire!",                                   errorType:"security"},
    "1_1_2_1" : {statusCode:401, msg:"AWS 'aud' client id invalid!",                        errorType:"input"},
    "1_1_2_2" : {statusCode:401, msg:"AWS 'iss' group URI invalid!",                        errorType:"input"},
    "1_1_2_3" : {statusCode:401, msg:"AWS 'token use' invalid!",                            errorType:"input"},

    "1_1_3_0" : {statusCode:500, msg:"error on calling public JWT key REST services"},
    "1_1_3_1" : {statusCode:500, msg:"unable to grab public JWK information"},
    "1_1_3_2" : {statusCode:500, msg:"Public JWK information doesn't contains keys"},
    "1_1_3_3" : {statusCode:401, msg:"Invalid kid!",                                        errorType:"security"},
    "1_1_3_4" : {statusCode:401, msg:"Invalid algorithm!",                                  errorType:"security"},


    // ============================================================================
    // entities Entity Id Entitlements
    // ============================================================================
    "1_2_1_0" : {statusCode:400, msg:"header is missing",                                   errorType:"input"},
    "1_2_1_1" : {statusCode:400, msg:"header Authorization is missing",                     errorType:"input"},
    "1_2_1_2" : {statusCode:400, msg:"path parameters is missing",                          errorType:"input"},
    "1_2_1_3" : {statusCode:400, msg:"sourceId is missing",                                 errorType:"input"},
    "1_2_1_4" : {statusCode:400, msg:"entityId is missing",                                 errorType:"input"},
    "1_2_1_4" : {statusCode:400, msg:"type is missing",                                     errorType:"input"},

    "1_2_2_0" : {statusCode:500, msg:"Error on calling dynamoDB for search user information"},
    "1_2_2_1" : {statusCode:403, msg:"user not found",failBack : {"userHasTheRequestedPrivilege": false } ,         errorType:"functionnal"},
    "1_2_2_2" : {statusCode:403, msg:"user has any entitlement",failBack :{"userHasTheRequestedPrivilege": false }, errorType:"functionnal"},
    "1_2_2_3" : {statusCode:500, msg:"Error on calling dynamoDB for search entitlements"},
    "1_2_2_4" : {statusCode:500, msg:"Unsynchronized user and entitlements informations"},
    "1_2_2_5" : {statusCode:500, msg:"Error on filtering entitlements"},


    // ============================================================================
    // add M3 Id To IdToken
    // ============================================================================
    "1_3_0_0" : {statusCode:500, msg:"config.addM3IdToIdToken_appliesToTriggerSources is required", errorType:"input"},
    "1_3_0_1" : {statusCode:500, msg:"config.userPoolId is required",                               errorType:"input"},

    "1_3_1_0" : {statusCode:500, msg:"Not the expected userPoolId!"},
    "1_3_1_1" : {statusCode:500, msg:"Not adding the m3_id"},


    // ============================================================================
    // GET /entitlements
    // ============================================================================
    "1_4_0_0" : {statusCode:500, msg:"header is missing",                   errorType:"input"},
    "1_4_0_1" : {statusCode:500, msg:"header Authorization is missing" ,    errorType:"input"},
    
    "1_4_1_0" : {statusCode:500, msg:"Error on calling dynamoDB for search user information"},
    "1_4_1_1" : {statusCode:404, msg:"user not found", "failBack" : [] ,                errorType:"functionnal"},
    "1_4_1_2" : {statusCode:404, msg:"user has any entitlement", "failBack" : [],       errorType:"functionnal"},
    "1_4_1_3" : {statusCode:500, msg:"Error on calling dynamoDB for search entitlements"},
    "1_4_1_4" : {statusCode:500, msg:"Unsynchronized user and entitlements informations"},
    "1_4_1_5" : {statusCode:500, msg:"Error on filtering entitlements"},

    
    // ============================================================================
    // CHECK CURRENT USER IS ADMIN
    // ============================================================================
     "1_5_0_0" : {statusCode:500, msg:"Error can't create cognito client!"},
     "1_5_0_1" : {statusCode:403, msg:"no account id define in request"},
     "1_5_1_0" : {statusCode:403, msg:"error on calling cognito"},
     "1_5_2_0" : {statusCode:403, msg:"user isn't adminitrator!"},

    // ============================================================================
    // CHECK CURRENT USER IS ADMIN
    // ============================================================================
    "1_6_0_0" : {statusCode:400, msg:"body input is missing!"},
    "1_6_0_1" : {statusCode:400, msg:"input json not correcly formated!"},
    "1_6_1_0" : {statusCode:500, msg:"error on saving entitlements"},
}


//=======================================================================================
module.exports = {
    "errors":errors,
    search : (errorCode)=>{
        let result = {statusCode:500, msg:"Technical error", errorCode:"0"};

        if(errorCode!=undefined && errorCode!=null){
            let managedError = errors[errorCode];
            if(managedError != null){
               result =  {
                  errorCode  : errorCode,
                  statusCode : managedError.statusCode,
                  msg        : managedError.msg,
                  failBack   : managedError.failBack==undefined?null:managedError.failBack,
                  errorType  : managedError.errorType==undefined?'technical':managedError.errorType
               }
            }
        }
        
        return result;
    }
};
