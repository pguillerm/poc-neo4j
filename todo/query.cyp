MATCH (currentProduct:Product) where id(currentProduct)=12
MATCH (currentProduct)-[:HAS_CATEGORY]->(category:ProductCategory)
OPTIONAL MATCH (currentProduct)-[:HAS_TAG]->(tag:Tag)
OPTIONAL MATCH (sameTagProduct)-[:HAS_TAG]->(tag)
OPTIONAL MATCH (user)-[:BUY]->(userProductByTag:Product)-[:HAS_TAG]->(tag:Tag)
OPTIONAL MATCH (user)-[:BUY]->(userProductByCategory:Product)-[:HAS_CATEGORY]->(category)
WITH [userProductByTag,userProductByCategory,sameTagProduct] as ns
CALL apoc.refactor.mergeNodes(ns) YIELD resultProducts
WITH  collect(distinct resultProducts.uid) AS otherProducts
UNWIND otherProducts AS otherProductUid
MATCH (otherProduct:Product) where otherProduct.uid=otherProductUid and not id(otherProduct) =12
MATCH (otherProduct)-[r:HAS_CATEGORY]->(otherCategory:ProductCategory)
OPTIONAL MATCH (otherProduct)-[:HAS_TAG]->(otherProductTag:Tag)
RETURN otherProduct,r,otherCategory limit 5




MATCH (book:Product)-[]->(bookCategory:ProductCategory) where bookCategory.name="book"
RETURN apoc.coll.unionAll(collect(book.uid), [3,4,5,6,7]) AS output