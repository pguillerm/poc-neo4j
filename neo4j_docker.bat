SET currentPath=%~dp0
SET currentNeo4jPath=%currentPath%neo4j

echo  %currentNeo4jPath%

IF NOT EXIST %currentNeo4jPath%\data (
    md %currentNeo4jPath%\data
) 
if not exist %currentNeo4jPath%\logs (
    md %currentNeo4jPath%\logs 
)
if not exist %currentNeo4jPath%\import (
    md %currentNeo4jPath%\import
)
if not exist %currentNeo4jPath%\conf  (
    md %currentNeo4jPath%\conf 
)
if not exist %currentNeo4jPath%\lib  (
    md %currentNeo4jPath%\lib 
)


docker run --name neo4j  -p7474:7474 -p7687:7687 -d -v %currentNeo4jPath%\data:/data -v %currentNeo4jPath%\logs:/var/lib/neo4j/logs -v %currentNeo4jPath%\import:/var/lib/neo4j/import -v %currentNeo4jPath%\conf:/var/lib/neo4j/conf -v %currentNeo4jPath%\plugins:/var/lib/neo4j/plugins --env NEO4J_AUTH=neo4j/test  neo4j:latest
