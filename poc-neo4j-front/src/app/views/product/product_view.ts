import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductsServices } from './../../services/products_services';
import { Product } from './../../models/entities';


@Component({
    templateUrl: './product_view.html',
    styleUrls: ['./product_view.scss']
})
export class ProductView {
    /**************************************************************************
     * ATTRIBUTES
     **************************************************************************/
    private category :string;
    private product: Product;

    private recommandedProducts : Product[];
    /**************************************************************************
     * CONSTRUCTOR
     **************************************************************************/
    constructor(private route: ActivatedRoute,
        private productsServices: ProductsServices) {

    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.category = params['categoryName'];
            let productId = params['productId'];
            this.productsServices.getProduct(productId)
                                 .then(product=>{
                                    this.product=product;
                                    this.searchRecommandedProducts(productId);

                                 })
                                 .catch(error=>{
                                     console.error(error);
                                 })
        });
    }

    private searchRecommandedProducts(productId:number){
        this.productsServices.getRecommendedProducts(productId)
        .then(products=>{
            this.recommandedProducts=products;

         })
         .catch(error=>{
             console.error(error);
         });
    }


    /**************************************************************************
     * BUILDER
     **************************************************************************/
    buildProductLink(product: Product){

        let category = "noCategory";
        if(product.productCategories && product.productCategories.length>0){
            category = product.productCategories[0].name;
        }
        return ['/category',category,product.id ];
    }
}