import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CategoriesServices } from './../../services/categories_services';
import {Product} from './../../models/entities';
@Component({
    templateUrl: './category_view.html',
    styleUrls: ['./category_view.scss']
})
export class CategoryView implements OnInit {
    /**************************************************************************
     * ATTRIBUTES
     **************************************************************************/
    private category: string;
    private products : Product[];

    /**************************************************************************
     * CONSTRUCTOR
     **************************************************************************/
    constructor(private route: ActivatedRoute,
        private categoriesServices: CategoriesServices) {

    }

    ngOnInit() {
        this.route.params.subscribe(params => {

            this.category = params['categoryName'];
            this.categoriesServices.getCategorysProducts(this.category)
                                    .then(products=>this.products=products);
        });
    }
}