import { Injectable } from "@angular/core";
import { environment } from "./../../environments/environment";
import {
    HttpClient,
    HttpResponse,
    HttpHeaders,
    HttpRequest
} from "@angular/common/http";

@Injectable()
export class ProductsServices {
    /***************************************************************************
     * CONSTRUCTORS
     ***************************************************************************/
    constructor(private http: HttpClient) { }

    /***************************************************************************
     * API
     ***************************************************************************/
    public getProduct(productId: number): Promise<any> {
        const options = {};
        const url = `${environment.urls.api}/products/${productId}`;
        return this.http.get(url, { headers: options, observe: "response" })
                        .toPromise()
                        .then(res => {
                            return res.body;
                        })
                        .catch(this.handleError);
    }

    public getRecommendedProducts(productId: number): Promise<any> {
        const options = {};
        const url = `${environment.urls.api}/products/${productId}/recommandations`;
        return this.http.get(url, { headers: options, observe: "response" })
                        .toPromise()
                        .then(res => {
                            return res.body;
                        })
                        .catch(this.handleError);
    }



    /***************************************************************************
     * ERRORS
     ***************************************************************************/
    private handleError(error: any): Promise<any> {
        let errorData = {
            status: error.status || 500,
            statusText: error.statusText || "error",
            url: error.url,
            data: error._body != null ? JSON.stringify(error._body) : null
        };

        return Promise.reject(errorData);
    }
}
