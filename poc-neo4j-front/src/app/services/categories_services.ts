import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import {
    HttpClient, HttpResponse,
    HttpHeaders, HttpRequest
} from '@angular/common/http';



@Injectable()
export class CategoriesServices {

    /***************************************************************************
    * CONSTRUCTORS
    ***************************************************************************/
    constructor(private http: HttpClient) {
    }

    /***************************************************************************
    * API
    ***************************************************************************/
    public getCategories(): Promise<any> {
        const options = {}
        const url = `${environment.urls.api}/categories`;
        return this.http.get(url, { "headers": options, observe: "response" })
            .toPromise()
            .then(res => {
                return res.body;
            })
            .catch(this.handleError);
    }

    public getCategorysProducts(categoryName:string): Promise<any> {
        const options = {}
        const url = `${environment.urls.api}/categories/${categoryName}/products`;
        return this.http.get(url, { "headers": options, observe: "response" })
            .toPromise()
            .then(res => {
                return res.body;
            })
            .catch(this.handleError);
    }

    /***************************************************************************
     * ERRORS
     ***************************************************************************/
    private handleError(error: any): Promise<any> {
        let errorData = {
            "status": error.status || 500,
            "statusText": error.statusText || "error",
            "url": error.url,
            "data": error._body!=null?JSON.stringify(error._body):null
        }

        return Promise.reject(errorData);
    }

}
