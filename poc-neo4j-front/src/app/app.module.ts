import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { HomeView } from './views/home/home_view';
import { CategoryView } from './views/category/category_view';
import { ProductView } from './views/product/product_view';
import { NotFoundView } from './views/not_found/not_found_view';

import { CategoriesServices } from './services/categories_services';
import { ProductsServices } from './services/products_services';

import { StarsComponent } from './components/stars/stars_component';


@NgModule({
  declarations: [
    AppComponent,

    HomeView,
    CategoryView,
    ProductView,
    NotFoundView,

    StarsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [CategoriesServices, ProductsServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
