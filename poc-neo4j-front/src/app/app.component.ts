import { Component, OnInit } from '@angular/core';
import { CategoriesServices } from './services/categories_services';

import {Category} from './models/entities';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  /***************************************************************************
  * ATTRIBUTES
  ***************************************************************************/
  private title = 'poc-neo4j-front';
  private categories: Category[];

  /***************************************************************************
  * CONSTRUCTORS
  ***************************************************************************/
  constructor(private categoriesServices: CategoriesServices) {
  }

  ngOnInit() {
    this.categoriesServices.getCategories()
      .then(data => {
        this.categories = data;
        console.log(this.categories);
      })
      .catch(error => {
        console.error(error);
      });
  }
}
