import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeView} from './views/home/home_view';
import {ProductView} from './views/product/product_view';
import {CategoryView} from './views/category/category_view';
import {NotFoundView} from './views/not_found/not_found_view';


const routes: Routes = [
  { path: 'category/:categoryName/:productId', component:ProductView},
  { path: 'category/:categoryName', component:CategoryView},
  { path: ''  , component: HomeView, pathMatch: 'full'},
  { path: '**', component:NotFoundView},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
