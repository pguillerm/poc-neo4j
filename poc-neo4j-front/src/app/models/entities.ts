export interface User{
    id          : number,
    firstName   : string,
    lastName    : string,
    name        : string,
    old         : number
}


export interface Category{
    id      : number,
    name    : string
}

export interface Tag{
    id      : number,
    name    : string
}


export interface Product{
    id                : number,
    uid               : number
    name              : string,
    price             : number,
    stars             : number,
    image             : string,
    imageType         : string,
    productCategories : Category[],
    tags              : Tag[],
    users             : User[]
}