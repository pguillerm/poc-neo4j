
import { Component, Input, OnInit } from '@angular/core';
import { faStar } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 's-stars',
    template: `
        <div [class]="s-stars" *ngIf="value">
            <div class="stars">
                <div class="star" *ngFor="let index of  [].constructor(value)">
                     <fa-icon [icon]="icon"></fa-icon>
                </div>
            </div>
            <div class="value">{{value}}</div>
        </div>
    `,
    styleUrls: ['./stars_component.scss']
})
export class StarsComponent {
    /**************************************************************************
      * ATTRIBUTES
      **************************************************************************/
    private icon: any = faStar;
    @Input() value: number;



}