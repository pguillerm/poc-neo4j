#!/bin/bash
currentPath=`dirname $(readlink -f "$BASH_SOURCE")`
currentNeo4jPath=$currentPath/neo4j

if [[ ! -e $currentNeo4jPath/data ]]; then
    mkdir $currentNeo4jPath/data
fi
if [[ ! -e $currentNeo4jPath/logs ]]; then
    mkdir $currentNeo4jPath/logs
fi
if [[ ! -e $currentNeo4jPath/import ]]; then
    mkdir $currentNeo4jPath/import
fi
if [[ ! -e $currentNeo4jPath/conf ]]; then
    mkdir $currentNeo4jPath/conf
fi


docker run --name neo4j  \
           -p7474:7474 -p7687:7687 \
           -d \
           -v $currentNeo4jPath/data:/data \
           -v $currentNeo4jPath/logs:/logs \
           -v $currentNeo4jPath/import:/var/lib/neo4j/import \
           -v $currentNeo4jPath/conf:/conf \
           neo4j:3.5.8