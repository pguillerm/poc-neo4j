package com.smartwave.tools.poc.neo4j.rest;

import com.smartwave.tools.poc.neo4j.dao.ProductsRepository;
import com.smartwave.tools.poc.neo4j.dao.ProductsServices;
import com.smartwave.tools.poc.neo4j.dao.entities.Product;
import com.smartwave.tools.poc.neo4j.dao.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestProducts {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private ProductsRepository productsRepository;
    @Autowired
    private ProductsServices   productsServices;

    // =========================================================================
    // API
    // =========================================================================
    @RequestMapping(path = "/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProducts() throws Exception {
        return productsServices.findAll();
    }

    @RequestMapping(path = "/products/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Product getProduct(@PathVariable("id") Long id) throws Exception {
        return productsRepository.findById(id).orElse(null);
    }


    @RequestMapping(path = "/products/{id}/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> usersWhoHasBoughtProduct(@PathVariable("id") Long id) throws Exception {
        return productsServices.findUsersByProduct(id);
    }

    @RequestMapping(path = "/products/{id}/recommandations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> recommandedProducts(@PathVariable("id") Long id,
                                             @RequestHeader(name="user", required=false) String user) throws Exception {

        return user==null|| "".equals(user.trim())
                ? productsRepository.findRecommendation(id)
                : productsRepository.findRecommendationForUser(id,user);
    }


}
