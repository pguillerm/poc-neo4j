package com.smartwave.tools.poc.neo4j.dao;

import com.smartwave.tools.poc.neo4j.dao.entities.Product;
import com.smartwave.tools.poc.neo4j.dao.entities.ProductCategory;
import com.smartwave.tools.poc.neo4j.dao.entities.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepository extends Neo4jRepository<ProductCategory, Long> {


    @Query("MATCH (product:Product)-[]->(category:ProductCategory) WHERE category.name={category} RETURN product")
    List<Product> findProductByCategory(@Param("category") String category);

}
