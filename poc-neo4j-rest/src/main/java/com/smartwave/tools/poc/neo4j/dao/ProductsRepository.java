package com.smartwave.tools.poc.neo4j.dao;

import com.smartwave.tools.poc.neo4j.dao.entities.Product;
import com.smartwave.tools.poc.neo4j.dao.entities.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductsRepository extends Neo4jRepository<Product, Long> {

    @Query("MATCH (product:Product)<-[:BUY]-(user:User) WHERE id(product)={id} RETURN user")
    List<User> usersByProduct(@Param("id") long id);

    @Query( "MATCH (currentProduct:Product) where currentProduct.uid={productId}" +
            " MATCH (currentProduct)-[:HAS_CATEGORY]->(category:ProductCategory)" +
            " OPTIONAL MATCH (user:User)-[:BUY]->(currentProduct) where not user.name = {user}" +
            " OPTIONAL MATCH (currentProduct)-[:HAS_TAG]->(tag:Tag)" +
            " OPTIONAL MATCH (user)-[:BUY]->(userProductByTag:Product)-[:HAS_TAG]->(tag:Tag)" +
            " OPTIONAL MATCH (user)-[:BUY]->(userProductByCategory:Product)-[:HAS_CATEGORY]->(category)" +
            " WITH  collect(distinct userProductByTag.uid) AS otherProducts" +
            " UNWIND otherProducts AS otherProductUid" +
            " MATCH (otherProduct:Product) where otherProduct.uid=otherProductUid and not otherProduct.uid = {productId}" +
            " MATCH (otherProduct)-[r:HAS_CATEGORY]->(otherCategory:ProductCategory)" +
            " OPTIONAL MATCH (otherProduct)-[:HAS_TAG]->(otherProductTag:Tag)" +
            " RETURN otherProduct,r,otherCategory limit 5")
    List<Product> findRecommendationForUser(@Param("productId") long productId, @Param("user") String currentUser);

    @Query( "MATCH (currentProduct:Product) where currentProduct.uid={productId}" +
            " MATCH (currentProduct)-[:HAS_CATEGORY]->(category:ProductCategory)" +
            " OPTIONAL MATCH (currentProduct)-[:HAS_TAG]->(tag:Tag)" +
            " OPTIONAL MATCH (user)-[:BUY]->(userProductByTag:Product)-[:HAS_TAG]->(tag:Tag)" +
            " OPTIONAL MATCH (user)-[:BUY]->(userProductByCategory:Product)-[:HAS_CATEGORY]->(category)" +
            " WITH  collect(distinct userProductByTag.uid) AS otherProducts" +
            " UNWIND otherProducts AS otherProductUid" +
            " MATCH (otherProduct:Product) where otherProduct.uid=otherProductUid and not otherProduct.uid = {productId}" +
            " MATCH (otherProduct)-[r:HAS_CATEGORY]->(otherCategory:ProductCategory)" +
            " OPTIONAL MATCH (otherProduct)-[:HAS_TAG]->(otherProductTag:Tag)" +
            " RETURN otherProduct,r,otherCategory limit 5")
    List<Product> findRecommendation(@Param("productId") long productId);

}
