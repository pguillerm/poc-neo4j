package com.smartwave.tools.poc.neo4j.dao;

import com.smartwave.tools.poc.neo4j.dao.entities.Product;
import com.smartwave.tools.poc.neo4j.dao.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductsServices {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final int DEPTH = 1;

    @Autowired
    private ProductsRepository productsRepository;

    // =========================================================================
    // API
    // =========================================================================
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        final List<Product>     result = new ArrayList<>();
        final Iterable<Product> data   = productsRepository.findAll();
        if (data != null) {
            data.forEach(result::add);
        }
        return result;
    }


    public List<User> findUsersByProduct(final long id){
        return productsRepository.usersByProduct(id);
    }
}
