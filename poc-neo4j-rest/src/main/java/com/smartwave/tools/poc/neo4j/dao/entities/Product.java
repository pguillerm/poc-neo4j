package com.smartwave.tools.poc.neo4j.dao.entities;

import org.neo4j.ogm.annotation.*;

import java.io.Serializable;
import java.util.Set;

@NodeEntity
public class Product implements Serializable {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = 6761302393435530309L;

    @Id
    @GeneratedValue
    private Long id;

    @Property
    private String name;

    @Property
    private double price;

    @Property
    private double stars;

    @Property
    private long uid;

    @Property
    private String image;

    @Property
    private String imageType;

    @Relationship(type = "HAS_CATEGORY")
    public Set<ProductCategory> productCategories;

    @Relationship(type = "HAS_TAG")
    public Set<Tag> tags;

    @Relationship(type = "BUY",direction = Relationship.INCOMING)
    public Set<User> users;

    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Product that = (Product) o;

        return uid == that.uid;
    }

    @Override
    public int hashCode() {
        return (int) (uid ^ (uid >>> 32));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductEntity{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append(", starts=").append(stars);
        sb.append(", uid=").append(uid);
        sb.append('}');
        return sb.toString();
    }

    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public double getStars() {
        return stars;
    }

    public void setStars(final double stars) {
        this.stars = stars;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(final long uid) {
        this.uid = uid;
    }

    public Set<ProductCategory> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(final Set<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(final Set<Tag> tags) {
        this.tags = tags;
    }


    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(final Set<User> users) {
        this.users = users;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(final String imageType) {
        this.imageType = imageType;
    }
}
