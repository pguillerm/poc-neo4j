package com.smartwave.tools.poc.neo4j.rest;

import com.smartwave.tools.poc.neo4j.dao.CategoryRepository;
import com.smartwave.tools.poc.neo4j.dao.entities.Product;
import com.smartwave.tools.poc.neo4j.dao.entities.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RestCategory {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private CategoryRepository categoryRepository;

    // =========================================================================
    // API
    // =========================================================================
    @RequestMapping(path = "/categories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductCategory> getCategories() throws Exception {
        final List<ProductCategory> result = new ArrayList<>();
        final Iterable<ProductCategory> resultSet = categoryRepository.findAll();


        if(resultSet!=null){
            resultSet.forEach(result::add);
        }

        return result;
    }



    @RequestMapping(path = "/categories/{categoryName}/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> findCategoryProducts(@PathVariable("categoryName") String categoryName) throws Exception {
        return categoryRepository.findProductByCategory(categoryName);
    }



}
