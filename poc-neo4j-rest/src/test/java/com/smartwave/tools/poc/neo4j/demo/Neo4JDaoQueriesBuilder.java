package com.smartwave.tools.poc.neo4j.demo;

public final class Neo4JDaoQueriesBuilder {

    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    Neo4JDaoQueriesBuilder(){
    }



    // =========================================================================
    // API
    // =========================================================================
    public String buildFindCategories() {
        return "MATCH (c:ProductCategory) RETURN c";
    }

}
