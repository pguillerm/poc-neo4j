package com.smartwave.tools.poc.neo4j.generators;

import org.inugami.api.loggers.Loggers;
import org.inugami.api.models.data.DataGeneratorUtils;
import org.junit.jupiter.api.Test;

public class DataGeneratorTest {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================


    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================


    // =========================================================================
    // API
    // =========================================================================
    @Test
    public void generateOrders() {
        final String[] users = {
                "durandana.des.meaux@rhyta.com",
                "AubreyLesage@rhyta.com",
                "granville.denis@jourrapide",
                "OliverRancourt@jourrapide.com",
                "russell.fecteau@rhyta.com",
                "lefebvre@rhyta.com"
        };

        final StringBuilder csv = new StringBuilder();
        for (String user : users) {
            for (int i = DataGeneratorUtils.getRandomBetween(1, 40); i >= 0; i--) {
                final long timestamps = ((long) DataGeneratorUtils.getRandomBetween(1546300800, 1569997853)) * 1000;
                final int  productId  = DataGeneratorUtils.getRandomBetween(1, 44);
                final int  nbProducts = DataGeneratorUtils.getRandomBetween(1, 4);

                csv.append(user).append(',');
                csv.append(productId).append(',');
                csv.append(timestamps).append(',');
                csv.append(nbProducts).append('\n');
            }
        }

        Loggers.APPLICATION.info("\n{}", csv);
    }

    // =========================================================================
    // OVERRIDES
    // =========================================================================


    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================

}
