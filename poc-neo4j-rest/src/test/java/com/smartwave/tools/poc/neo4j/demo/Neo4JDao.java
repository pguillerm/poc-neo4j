package com.smartwave.tools.poc.neo4j.demo;

import org.inugami.api.loggers.Loggers;
import org.neo4j.driver.internal.value.NodeValue;
import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.types.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.neo4j.driver.v1.Values.parameters;

public class Neo4JDao {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private final Driver                 driver;
    private final Neo4JDaoQueriesBuilder queriesBuilder;

    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public Neo4JDao(final String url,
                    final String login,
                    final String password) {
        Objects.requireNonNull(url, "Neo4J bolt url is mandatory!");
        Objects.requireNonNull(login, "Neo4J login url is mandatory!");
        Objects.requireNonNull(password, "Neo4J password url is mandatory!");

        driver         = GraphDatabase.driver(url, AuthTokens.basic(login, password));
        queriesBuilder = new Neo4JDaoQueriesBuilder();
    }

    // =========================================================================
    // API
    // =========================================================================
    public List<Category> findCategories() {
        try (final Session session = driver.session()) {
            final List<Node> resultSet = session.readTransaction(this::processFindCategories);

            return resultSet.stream()
                    .map(this::mapCategory)
                    .collect(Collectors.toList());
        }
    }

    private List<Node> processFindCategories(final Transaction tx) {
        final StatementResult statement = tx.run(queriesBuilder.buildFindCategories());

        return statement.list()
                .stream()
                .map(record -> record.get(0))
                .filter(value -> value instanceof NodeValue)
                .map(Value::asNode)
                .collect(Collectors.toList());
    }

    private Category mapCategory(final Node node) {
        final Value property = node.get("name");
        return new Category(node.id(), property == null ? null : property.asString());
    }

    // =========================================================================
    // API
    // =========================================================================
    public NodeValue createCategory(final String name) {
        NodeValue result = null;
        try (final Session session = driver.session()) {
            result = session.writeTransaction(tx -> {
                        final StatementResult statementResult = tx.run("MERGE (c:ProductCategory {name:$name}) RETURN c",
                                parameters("name", name));

                        final List<Record> data = statementResult.list();

                        NodeValue node = null;
                        if (!data.isEmpty()) {
                            node = (NodeValue) data.get(0).get(0);
                        }
                        return node;
                    }
            );
        }

        return result;
    }

    // =========================================================================
    //
    // =========================================================================
    public List<UserBuyProduct> findUsersWhoBuyProduct(final long id) {
        try (final Session session = driver.session()) {
            return session.readTransaction(tx -> processFindUsersWhoBuyProduct(id, tx));
        }
    }

    private List<UserBuyProduct> processFindUsersWhoBuyProduct(final long id, final Transaction tx) {
        final StatementResult statement = tx.run("MATCH (user:User)-[buy:BUY]->(product:Product) WHERE id(product)=$id RETURN user,buy",
                parameters(
                        "id", id
                ));

        final List<UserBuyProduct> result = new ArrayList<>();
        for (Record record : statement.list()) {
            final UserBuyProduct value      = mapUserBuyProduct(record);
            final int            valueIndex = result.indexOf(value);

            if (valueIndex == -1) {
                result.add(value);
            } else {
                result.get(valueIndex).mergeWhen(value.getWhen());
            }
        }

        return result;
    }

    private UserBuyProduct mapUserBuyProduct(final Record record) {
        final String userName     = record.get("user").get("name").asString();
        final Value  buy          = record.get("buy");
        final Value  dateProperty = buy.get("date");
        return new UserBuyProduct(userName, dateProperty == null ? null : dateProperty.asLong());
    }


}
