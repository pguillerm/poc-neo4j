package com.smartwave.tools.poc.neo4j.demo;

import java.util.ArrayList;
import java.util.List;

public class UserBuyProduct {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private final String     name;
    private final List<Long> when = new ArrayList<>();

    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public UserBuyProduct(final String userName) {
        name = userName;
    }

    public UserBuyProduct(final String userName, final Long date) {
        name = userName;
        if (date != null) {
            when.add(date);
        }
    }


    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final UserBuyProduct that = (UserBuyProduct) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserBuyProduct{");
        sb.append("name='").append(name).append('\'');
        sb.append(", when=").append(when);
        sb.append('}');
        return sb.toString();
    }

    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public String getName() {
        return name;
    }

    public List<Long> getWhen() {
        return when;
    }

    public void addWhen(final Long date) {
        if (date != null) {
            when.add(date);
        }
    }

    public void mergeWhen(final List<Long> when) {
        if (when != null) {
            this.when.addAll(when);
        }
    }
}
