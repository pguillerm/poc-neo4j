package com.smartwave.tools.poc.neo4j.demo;

public class Category {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private final Long   id;
    private final String name;

    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public Category(final String name) {
        this.name = name;
        this.id   = null;
    }

    public Category(final long id, final String name) {
        this.id   = id;
        this.name = name;
    }


    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public boolean equals(final Object object) {
        boolean result = this == object;
        if (!result && object != null && object instanceof Category) {
            final Category other = (Category) object;
            result = name == null ? other.getName()==null : name.equals(other.getName());
        }
        return result;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Category{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    // =========================================================================
    // GETTERS
    // =========================================================================
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
