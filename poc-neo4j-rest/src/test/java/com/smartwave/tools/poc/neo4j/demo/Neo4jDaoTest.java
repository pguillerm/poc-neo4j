package com.smartwave.tools.poc.neo4j.demo;


import org.inugami.api.loggers.Loggers;
import org.junit.jupiter.api.Test;

import java.util.List;

public class Neo4jDaoTest {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================


    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================


    // =========================================================================
    // API
    // =========================================================================
    @Test
    public void test() {
        /*
        final Neo4JDao       dao  = new Neo4JDao("bolt://127.0.0.1:7687", "neo4j", "test");
        final List<Category> data = dao.findCategories();
        Loggers.DEBUG.info("data : {}", data);
         */
    }

    @Test
    public void testRelation() {
        /*
        final Neo4JDao             dao  = new Neo4JDao("bolt://127.0.0.1:7687", "neo4j", "test");
        final List<UserBuyProduct> data = dao.findUsersWhoBuyProduct(14L);
        Loggers.DEBUG.info("data : {}", data);
         */
    }


    @Test
    public void testCreateNode() {
        /*
        final Neo4JDao  dao  = new Neo4JDao("bolt://127.0.0.1:7687", "neo4j", "test");
        final NodeValue node = dao.createCategory("foobar");
        Loggers.DEBUG.info("node : {}", node);
        */
    }


    // =========================================================================
    // OVERRIDES
    // =========================================================================


    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================

}
